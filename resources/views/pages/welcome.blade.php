@extends('layout.master')

@section('title')
Halaman Index
@endsection

@section('content')
<h1>SELAMAT DATANG! {{ $fullName }}</h1>
<h2>Terima kasih telah bergabung di Website Kami. Media Belajar Kita Bersama!</h2>
@endsection