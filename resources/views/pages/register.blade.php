@extends('layout.master')

@section('title')
Buat Account Baru
@endsection
    
@section('content')
<h2>Sign Up Form</h2>

<form action="/welcome" method="post">

    @csrf

    <label for="firstName">First Name : </label><br><br>
    <input type="text" id="firstName" name="firstName"><br><br>

    <label for="lastName">Last Name : </label><br><br>
    <input type="text" id="lastName" name="lastName"><br><br>

    <label for="gender">Gender</label><br><br>

    <input type="radio" name="gender" id="male" value="male">
    <label for="male">Male</label><br>

    <input type="radio" name="gender" id="female" value="female">
    <label for="female">Female</label><br><br>

    <label for="nationality">Nationality</label><br><br>

    <select name="nationality" id="nationality">
        <option value="wni" selected>Indonesia</option>
        <option value="wna">Warga Negara Asing</option>
    </select><br><br>

    <label for="language">Language Spoken</label><br><br>
    <input type="checkbox" name="language1" id="language1" value="bahasa indonesia">
    <label for="language1">Bahasa Indonesia</label><br>
    <input type="checkbox" name="language2" id="language2" value="english">
    <label for="language1">English</label><br>
    <input type="checkbox" name="language3" id="language3" value="other">
    <label for="language1">Other</label><br><br>

    <label for="bio">Bio</label><br><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>

    <input type="submit" value="Sign Up">

</form>
@endsection